Commerce G2s Payment Gateway
---------------------------------------------------

Commerce G2S is a payment gateway module for Gate2Shop payment gateway.
Working of this module is similar to Paypal WPS
Gate2Shop only have offsite payment
You can refer the integration document for reference.
Gate2Shop also have DMN (Direct Merchant Notification) to confirm the
payment. More details about DMN is available in Gate2Shop DMN
notification

This module allows users to set Gate2Shop credentials in the payment
method settings form present in payment method rule.

This module does not change the order status.

This module only creates a payment transaction object with transaction
status and remote transaction ID

We can use commerce checkout rules for successful order status change
and mail sending.
